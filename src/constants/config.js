const NOTI_EMPTY_PRODUCT = "Empty product in your cart";
const NOTI_GREATER_THAN_ONE = {
  group: "notification",
  type: "warn",
  text: "Quantity must be greater than 1"
};
const NOTI_ACT_ADD = {
  group: "notification",
  type: "success",
  text: "Add product succeed !!"
};
const NOTI_ACT_UPDATE = "Update product succeed !!";
const NOTI_ACT_DELETE = "Delete product succeed !!";

export {
  NOTI_EMPTY_PRODUCT,
  NOTI_ACT_ADD,
  NOTI_ACT_DELETE,
  NOTI_ACT_UPDATE,
  NOTI_GREATER_THAN_ONE
};
