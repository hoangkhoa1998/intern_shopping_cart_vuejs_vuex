import mutations from "../product/mutations";
import actions from "../product/actions";
import getters from "../product/getters";
import state from "../product/state";

export default {
  state,
  actions,
  getters,
  mutations
};
