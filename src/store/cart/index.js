import mutations from "../cart/mutations";
import actions from "../cart/actions";
import getters from "../cart/getters";
import state from "../cart/state";

export default {
  state,
  actions,
  getters,
  mutations
};
