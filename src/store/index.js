import Vue from "vue";
import Vuex from "vuex";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";
import state from "./state";
import moduleCart from "./cart";
import moduleProduct from "./product";
Vue.use(Vuex);

const store = new Vuex.Store({
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
  modules: {
    cart: moduleCart,
    product: moduleProduct
  }
});

export default store;
