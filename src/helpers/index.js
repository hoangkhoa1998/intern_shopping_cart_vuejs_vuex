const toCurrency = function(value, unit = "USD", direction = "right") {
  if (direction === "right") {
    return value + "" + unit;
  } else return unit + "" + value;
};

const validateQuantity = value => {
  let number = parseInt(value);
  if (number === value && !isNaN(number)) {
    return number >= 1;
  }

  return false;
};

export { toCurrency, validateQuantity };
